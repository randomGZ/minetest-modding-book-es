---
title: Controllo automatico degli errori
layout: default
root: ../..
idx: 8.2
description: Usa LuaCheck per trovare eventuali errori
redirect_from: /it/chapters/luacheck.html
---

## Introduzione <!-- omit in toc -->

In questo capitolo, imparerai come usare uno strumento chiamato LuaCheck per scansionare automaticamente le tue mod alla ricerca di eventuali errori.
LuaCheck può essere usato in combinazione con l'editor per fornire avvertimenti vari.

- [Installare LuaCheck](#installare-luacheck)
  - [Windows](#windows)
  - [Linux](#linux)
- [Eseguire LuaCheck](#eseguire-luacheck)
- [Configurare LuaCheck](#configurare-luacheck)
  - [Risoluzione problemi](#risoluzione-problemi)
- [Uso nell'editor](#uso-nelleditor)

## Installare LuaCheck

### Windows

Basta scaricare luacheck.exe dall'apposita [pagina delle versioni su Github](https://github.com/mpeterv/luacheck/releases).

### Linux

Per prima cosa, avrai bisogno di installare LuaRocks:

    sudo apt install luarocks

Poi va installato globalmente LuaCheck:

    sudo luarocks install luacheck

Per controllare che sia stato installato correttamente, fai:

    luacheck -v

## Eseguire LuaCheck

La prima volta che si esegue LuaCheck, segnalerà probabilmente un sacco di falsi errori.
Questo perché ha ancora bisogno di essere configurato.

Su Windows, apri la powershell o la bash nella cartella principale del tuo progetto ed esegui `path\to\luacheck.exe .`

Su Linux, esegui `luacheck .` nella cartella principale del progetto.

## Configurare LuaCheck

Crea un file chiamato .luacheckrc nella cartella principale del tuo progetto.
Questa può essere quella di un gioco, di un pacchetto mod o di una mod singola.

Inserisci il seguente codice all'interno:

```lua
unused_args = false
allow_defined_top = true

globals = {
    "minetest",
}

read_globals = {
    string = {fields = {"split"}},
    table = {fields = {"copy", "getn"}},

    -- Builtin
    "vector", "ItemStack",
    "dump", "DIR_DELIM", "VoxelArea", "Settings",

    -- MTG
    "default", "sfinv", "creative",
}
```

Poi, avrai bisogno di assicurarti che funzioni eseguendo LuaCheck: dovresti ottenere molti meno errori questa volta.
Partendo dal primo errore, modifica il codice per risolvere il problema, o modifica la configurazione di LuaCheck se il codice è corretto.
Dài un occhio alla lista sottostante.

### Risoluzione problemi

* **accessing undefined variable foobar** - Se `foobar` dovrebbe essere una variabile globale, aggiungila a `read_globals`.
  Altrimenti, manca un `local` vicino a `foobar`.
* **setting non-standard global variable foobar** - Se `foobar` dovrebbe essere una variabile globale, aggiungila a `globals`.
  Rimuovila da `read_globals` se presente.
  Altrimenti, manca un `local` vicino a `foobar`.
* **mutating read-only global variable 'foobar'** - Sposta `foobar` da `read_globals` a `globals`, o smetti di modificare `foobar`.

## Uso nell'editor

È caldamente consigliato installare un'estensione per il tuo editor di fiducia che ti mostri gli errori senza eseguire alcun comando.
Queste sono disponibili nella maggior parte degli editor, come:

* **VSCode** - Ctrl+P, poi incolla: `ext install dwenegar.vscode-luacheck`;
* **Sublime** - Installala usando package-control:
        [SublimeLinter](https://github.com/SublimeLinter/SublimeLinter),
        [SublimeLinter-luacheck](https://github.com/SublimeLinter/SublimeLinter-luacheck).
